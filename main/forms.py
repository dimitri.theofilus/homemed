from .models import joinUs
from django import forms
from django.forms import fields

class JoinForm(forms.ModelForm):
    class Meta:
        model = joinUs
        fields = "__all__"