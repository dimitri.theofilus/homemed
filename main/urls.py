from django.urls import path
from . import views
from .views import add_joinUs,json

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('joinUs', add_joinUs, name='home'),
    path('json', json, name='json')
]
