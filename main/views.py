from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from .models import joinUs
from .forms import JoinForm
from django.core import serializers
from django.http import JsonResponse

def home(request):
    return render(request, 'main/home.html')

def add_joinUs(request):
    context = {}
    form = JoinForm(request.POST or None, request.FILES or None)
    if request.method == "POST":
        if form.is_valid:
            form.save()

    context['form']= form
    return render(request, "home.html", context)

def json(request):
    data = serializers.serialize('json' , joinUs.objects.all())
    return HttpResponse(data, content_type = "application/json")